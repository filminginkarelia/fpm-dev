FROM php:7.2-fpm
ARG TIMEZONE=Europe/Moscow

RUN apt-get update && apt-get install -y libpng-dev
RUN apt-get install -y openssl git unzip mc vim curl \
    libwebp-dev libjpeg62-turbo-dev libxpm-dev  libjpeg-dev libpng-dev libfreetype6-dev \
    libmemcached-dev libz-dev libpq-dev libssl-dev libmcrypt-dev \
    jpegoptim wget gnupg

# Install NodeJS and npm
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Set timezone
RUN ln -snf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && echo ${TIMEZONE} > /etc/timezone
RUN printf '[PHP]\ndate.timezone = "%s"\n', ${TIMEZONE} > /usr/local/etc/php/conf.d/tzone.ini

RUN docker-php-ext-configure gd \
#    --enable-gd-native-ttf \
    --with-gd \
    --with-webp-dir \
    --with-jpeg-dir=/usr/lib \
    --with-png-dir \
    --with-zlib-dir \
    --with-xpm-dir \
    --with-freetype-dir=/usr/include/freetype2

RUN docker-php-ext-install pdo pdo_mysql gd

# install xdebug
RUN pecl install xdebug && docker-php-ext-enable xdebug


# Configuration
RUN echo "error_reporting = E_ALL" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "display_startup_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "display_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_connect_back=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.idekey=\"PHPSTORM\"" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_port=9001" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

COPY php.ini ${PHP_INI_DIR}/php.ini

WORKDIR /var/www/symfony
